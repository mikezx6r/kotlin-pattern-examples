package com.bns.assertj

import org.assertj.core.api.AbstractThrowableAssert
import org.assertj.core.api.Assertions

inline fun <reified T> shouldThrow(noinline thrower: () -> Unit): AbstractThrowableAssert<*, *> =
    Assertions.assertThatThrownBy(thrower).isExactlyInstanceOf(T::class.java)

fun doesNotException(thrower: () -> Unit) =
    Assertions.assertThatCode(thrower).doesNotThrowAnyException()

