package com.bns.assertj

import org.assertj.core.api.AbstractCharSequenceAssert
import org.assertj.core.api.AssertionsForInterfaceTypes

infix fun String.shouldContain(items: String): AbstractCharSequenceAssert<*, *> =
    AssertionsForInterfaceTypes.assertThat(this).contains(items)

fun String.shouldBeEmpty() = AssertionsForInterfaceTypes.assertThat(this).isEmpty()
fun String.shouldBeBlank() = AssertionsForInterfaceTypes.assertThat(this).isBlank()
fun String?.shouldNotBeBlank() = AssertionsForInterfaceTypes.assertThat(this).isNotBlank()

infix fun String.shouldStartWith(start: String): AbstractCharSequenceAssert<*, *> =
    AssertionsForInterfaceTypes.assertThat(this).startsWith(start)
