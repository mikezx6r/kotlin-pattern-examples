package com.bns.assertj

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.AbstractObjectAssert
import org.assertj.core.api.ObjectAssert
import kotlin.reflect.KClass

inline infix fun <reified T : Any?> T.shouldEqual(parm: T): AbstractObjectAssert<*, *> =
    ObjectAssert<T>(this).isEqualTo(parm)

inline infix fun <reified T : Any?> T.shouldNotEqual(parm: T): AbstractObjectAssert<*, *> =
    ObjectAssert<T>(this).isNotEqualTo(parm)

fun <T : Any?> T.shouldBeNull() = ObjectAssert<T>(this).isNull()

fun <T : Any> T.shouldEqualIgnoringFields(other: T, vararg fieldsToIgnore: String):
        AbstractObjectAssert<*, *> = ObjectAssert<T>(this).isEqualToIgnoringGivenFields(other, *fieldsToIgnore)

infix fun <T : Any> T.shouldEqualRecursively(other: T):
        AbstractObjectAssert<*, *> = ObjectAssert<T>(this).isEqualToComparingFieldByFieldRecursively(other)

inline fun <reified T : Any> T.shouldNotHaveNullProperties():
        AbstractObjectAssert<*, *> = ObjectAssert<T>(this).hasNoNullFieldsOrProperties()

inline fun <reified T : Any> T.shouldNotHaveNullPropertiesExcept(vararg fieldsToIgnore: String):
        AbstractObjectAssert<*, *> = ObjectAssert<T>(this).hasNoNullFieldsOrPropertiesExcept(*fieldsToIgnore)

inline infix fun <reified T : Any> T.shouldBe(v: IsValues): AbstractAssert<*, *> {
    val assertThat = ObjectAssert<T>(this)
    return when (v) {
        IsValues.True -> assertThat.isEqualTo(true)
        IsValues.False -> assertThat.isEqualTo(false)
        IsValues.NonNull -> assertThat.isNotNull
    }
}

infix fun <T : Any> T.isOfType(kClass: KClass<*>): AbstractAssert<*, *> =
    ObjectAssert(this).isExactlyInstanceOf(kClass.java)

inline infix fun <reified T : Any> T.isNotOfType(type: KClass<*>): AbstractAssert<*, *> =
    ObjectAssert<T>(this).isNotExactlyInstanceOf(type.java)

