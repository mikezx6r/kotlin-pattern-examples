package com.bns.assertj

import org.assertj.core.api.Assertions
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.assertj.core.api.IterableAssert
import org.assertj.core.api.ListAssert
import org.assertj.core.api.MapAssert
import org.assertj.core.data.MapEntry

infix fun <E> List<E>.containsExactly(items: E): ListAssert<E> = assertThat(this).containsExactly(items)
infix fun <E> ListAssert<E>.containsExactly(items: E): ListAssert<E> = this.containsExactly(items)

infix fun <E> List<E>.hasSize(size: Int): ListAssert<E> = assertThat(this).hasSize(size)
infix fun <E> ListAssert<E>.hasSize(size: Int): ListAssert<E> = this.hasSize(size)

fun Collection<*>.shouldBeEmpty() = assertThat(this).isEmpty()
fun Map<*, *>.shouldBeEmpty() = assertThat(this).isEmpty()

infix fun <E> Collection<E>.shouldHaveSameElementsAs(items: Collection<E>): IterableAssert<E> =
    assertThat(this).hasSameElementsAs(items)

fun <T> List<T>.shouldEqualIgnoringFields(expected: List<T>, vararg fieldsToIgnore: String) {
    this.forEachIndexed { i, item ->
        Assertions.assertThat(item).isEqualToIgnoringGivenFields(expected[i], *fieldsToIgnore)
    }
}

infix fun <E> List<E>.contains(items: E): ListAssert<E> = assertThat(this).contains(items)
infix fun <E> ListAssert<E>.contains(items: E): ListAssert<E> = this.contains(items)

fun <K, V> Map<K, V>.containsOnly(vararg items: MapEntry<K, V>): MapAssert<K, V> =
    assertThat(this).containsOnly(*items)

fun <K, V> Map<K, V>.containsExactly(vararg items: MapEntry<K, V>): MapAssert<K, V> =
    assertThat(this).containsExactly(*items)
