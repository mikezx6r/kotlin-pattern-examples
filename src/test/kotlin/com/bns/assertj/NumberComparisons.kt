package com.bns.assertj

import org.assertj.core.api.AbstractBigDecimalAssert
import org.assertj.core.api.AbstractIntegerAssert
import org.assertj.core.api.AbstractLongAssert
import org.assertj.core.api.Assertions.assertThat
import java.math.BigDecimal

fun Int.shouldBeOne(): AbstractIntegerAssert<*> = assertThat(this).isOne()
fun Int.shouldBeZero(): AbstractIntegerAssert<*> = assertThat(this).isZero()
fun Int.shouldNotBeZero(): AbstractIntegerAssert<*> = assertThat(this).isNotZero()

fun Long.shouldBeOne(): AbstractLongAssert<*> = assertThat(this).isOne()
fun Long.shouldBeZero(): AbstractLongAssert<*> = assertThat(this).isZero()
fun Long.shouldNotBeZero(): AbstractLongAssert<*> = assertThat(this).isNotZero()

fun BigDecimal.shouldBeOne(): AbstractBigDecimalAssert<*> = assertThat(this).isOne()
fun BigDecimal.shouldBeZero(): AbstractBigDecimalAssert<*> = assertThat(this).isZero()
fun BigDecimal.shouldNotBeZero(): AbstractBigDecimalAssert<*> = assertThat(this).isNotZero()
