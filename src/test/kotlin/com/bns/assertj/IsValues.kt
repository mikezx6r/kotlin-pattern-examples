package com.bns.assertj

enum class IsValues {
    True,
    False,
    NonNull
}
