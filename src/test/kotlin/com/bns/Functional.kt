package com.bns

import java.math.BigDecimal
import java.time.LocalDateTime

// Extract EP Recon Data and Cloud Recon Data and Reconcile
// And return errors as string array
fun reconcileEpAndCloudData(cloudReconControl: CloudReconControl): List<String> {
    val cloudTotal: CloudReconTotal? =
        getCloudReconTotal(cloudReconControl.reconciliationId, cloudReconControl.periodStartTimestamp)
    val epTotal = getEpReconTotal(
        cloudReconControl.reconciliationId, cloudReconControl.periodStartTimestamp
    )

    // NullPointerException check still required in case of record not found in DB
    return when {
        // When both cloud and EP total is empty, then recon process status will be "COMPLETE"
        cloudTotal == null && epTotal == null -> emptyList()

        cloudTotal == null -> listOf(
            "NO CLOUD RECORD FOUND!!!" +
                    "[${cloudReconControl.reconciliationId},${cloudReconControl.periodStartTimestamp}]"
        )

        epTotal == null -> listOf(
            "NO EP RECORD FOUND!!!" +
                    "[${cloudReconControl.reconciliationId},${cloudReconControl.periodStartTimestamp}]"
        )

        else -> {
            val toVerify: List<Triple<String, Number, Number>> = listOf(
                Triple("Credit Amount", cloudTotal.totalCreditAmount, epTotal.creditAmt),
                Triple("Credit Count", cloudTotal.totalCreditCount, epTotal.creditCnt),
                Triple("Debit Amount", cloudTotal.totalDebitAmount, epTotal.debitAmt),
                Triple("Debit Count", cloudTotal.totalDebitCount, epTotal.debitCnt),
                Triple("Total Txn Count", cloudTotal.totalTransCount, epTotal.totalTransCnt)
            )

            toVerify.fold(emptyList()) { acc, (field, cloudValue, epValue) ->
                if (cloudValue != epValue) {
                    val error = "$field Error!!![${cloudReconControl.reconciliationId}," +
                            "${cloudReconControl.periodStartTimestamp}]-Cloud($cloudValue):" +
                            "EP($epValue)"

                    acc + error
                } else {
                    acc
                }
            }
        }
    }
}

fun getCloudReconTotal(reconciliationId: String, periodStartTimestamp: LocalDateTime): CloudReconTotal? {
    TODO("not implemented")
}

fun getEpReconTotal(reconciliationId: String, periodStartTimestamp: LocalDateTime): EPReconTotal? {
    TODO("not implemented")
}

data class CloudReconControl(val reconciliationId: String, val periodStartTimestamp: LocalDateTime)

data class CloudReconTotal(
    var reconciliationId: String = "",
    var periodStartTimestamp: LocalDateTime = LocalDateTime.MIN,
    var totalDebitAmount: BigDecimal = BigDecimal.ZERO,
    var totalDebitCount: Int = 0,
    var totalCreditAmount: BigDecimal = BigDecimal.ZERO,
    var totalCreditCount: Int = 0,
    var totalTransCount: Int = 0
)

data class EPReconTotal(
    var reconciliationId: String = "",
    var periodStartTs: LocalDateTime = LocalDateTime.MIN,
    var creditAmt: BigDecimal = BigDecimal.ZERO,
    var creditCnt: Int = 0,
    var debitAmt: BigDecimal = BigDecimal.ZERO,
    var debitCnt: Int = 0,
    var totalTransCnt: Int = 0
)
