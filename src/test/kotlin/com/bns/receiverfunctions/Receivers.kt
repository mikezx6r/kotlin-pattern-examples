package com.bns.receiverfunctions

import com.bns.assertj.shouldEqual
import com.bns.domain.SomeObject

fun alsoDemo() {
    val result = SomeObject().apply {
        chars = "some value"
        number = 12
    }

    // *** Create another and know the properties are getting set on correct object
    val result2 = SomeObject().apply {
        chars = "some value"
        number = 12
    }

    with(result) {
        println(chars)
        println(number)
    }

    val other = result.let {
        it.chars + it.number
    }
    other shouldEqual "some value 12"

    val something: SomeObject? = SomeObject()

    val maybeNullResult = something?.let {
        it.chars + it.number
    } ?: "was null"

    println(maybeNullResult)

    val another = result.run {
        // Perform some function that involves result. Result will be default target of any property/function calls
        Pair("first", 15)
    }.also {
        // To do something else with `it` as variable. Returns original object
        it.first + it.second shouldEqual "first15"
    }
}

// Builder pattern if required
class Receivers(var value: String) {
    fun someBuildFunction(newValue: String) = apply { value = newValue }
}
