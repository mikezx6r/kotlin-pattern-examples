package com.bns.sealed

import com.bns.assertj.IsValues.False
import com.bns.assertj.IsValues.True
import com.bns.assertj.contains
import com.bns.assertj.shouldBe
import com.bns.assertj.shouldBeEmpty
import com.bns.assertj.shouldEqual
import com.bns.sealed.ProcessResult.Failure
import com.bns.sealed.ProcessResult.Success
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.util.*

class ProcessResultOldTest {
    @Test
    @Disabled
    // Use ` so can use text as function name
    fun `verify Process Result Old`() {
        val success = doSomethingOld(true)

        // everything inside closure is called on object in 'success'. Success becomes receiver of calls
        with(success) {
            id shouldEqual "someId"
            successful shouldBe True
            errors.shouldBeEmpty()
        }

        val failure = doSomethingOld(false)

        with(failure) {
            id shouldEqual "someId"
            successful shouldBe False
            errors contains "ErrorValue"
        }
    }

    @Test
    fun `verify Process Result`() {
        val result: ProcessResult = doSomething()

        // Showing usage of helper function exposed like a property rather than a  function
        result.hasErrors

        // Note that exhaustive when for sealed class is only enforced IF when is used as an expression (i.e. return value is referenced)
        // Otherwise, exhaustive when is not validated.
        val result2 = when (result) {
            is Success -> {
                // do success processing

                // Kotlin automatically casts result to Success so can use OtherProperty even though it was a ProcessResult
                println(result.otherProperty)
            }
            is Failure -> {
                // do error handling

                // Kotlin automatically casts so have access to errors here. OtherProperty would be a compile error
                //  println(result.otherProperty)  !! COMPILE ERROR  !!
                println(result.errors)
            }
        }

    }

}

// *** Defines a true constant. Will get inlined by compiler
const val RESULT_ID = "someId"

// *** Provides get (set if a var), equals, hashcode, toString, copy and decomposition functions
data class ProcessResultOld(val id: String, val successful: Boolean, val errors: List<String> = emptyList())

// *** Defines an inheritance structure that is fixed based on this definition. ProcessResult cannot be extended anywhere else
sealed class ProcessResult(val id: String, val errors: List<String> = emptyList()) {
    // *** Defining a new helper function that looks like a property when used
    val hasErrors: Boolean
        get() = errors.isEmpty()

    class Success(id: String, val otherProperty: String) : ProcessResult(id)

    class Failure(id: String, errors: List<String>) : ProcessResult(id, errors)
}


fun doSomething(): ProcessResult =
    if (Random().nextInt() < .5) Success(RESULT_ID, "otherProperty")
    else Failure(
        RESULT_ID,
        listOf("Errorcode")
    )


fun doSomethingOld(pass: Boolean): ProcessResultOld =
    if (pass) ProcessResultOld(RESULT_ID, true, emptyList())
    else
        ProcessResultOld(RESULT_ID, false)

