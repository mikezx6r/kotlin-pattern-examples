package com.bns.loops

import com.bns.assertj.shouldEqual
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.of
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

// Demonstrates ranges, and creating a joined String with a built-in function
class LoopAndRanges {
    @ParameterizedTest
    @ArgumentsSource(RangeArguments::class)
    fun rangeDemonstration(range: IntProgression, expected: String) {
        range.joinToString() shouldEqual expected
    }

    class RangeArguments : ArgumentsProvider {
        override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
            of(1..4, "1, 2, 3, 4"),
            of(1 until 4, "1, 2, 3"),
            of(4 downTo 1, "4, 3, 2, 1"),
            of(1..8 step 2, "1, 3, 5, 7")
        )
    }

    @Test
    fun repeating() {
        var s = ""

        // Want to do something 10 times
        repeat(10) { s += "a" }

        // Want to do something that seems like a utility function. Odds are it exists in Kotlin already.
        s shouldEqual "a".repeat(10)
    }
}

