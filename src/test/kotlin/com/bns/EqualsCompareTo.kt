package com.bns

import com.bns.assertj.shouldEqual
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class EqualsCompareTo {
    @ParameterizedTest(name = "compare: {index} - {arguments}")
    @CsvSource("1, 2, false", "2, 1, true", "1, 1, true")
    fun managingCompareTo(first: Int, second: Int, expected: Boolean) {

        // IntelliJ suggest replacing this with first >= second
        (first.compareTo(second) >= 0) shouldEqual expected

        (first >= second) shouldEqual expected
    }
}
