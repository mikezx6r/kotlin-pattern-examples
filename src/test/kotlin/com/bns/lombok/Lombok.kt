package com.bns.lombok

import java.math.BigDecimal


//@Data
data class MyData(val id: String, val other: BigDecimal, var mutableProperty: String)


//@UtilityClass
object SomeUtility {
    fun someFunction() = "return"
}


// @RequiredArgsConstructor
class MyClass(val property: String, val nullableProperty: String?, val optionalParameter: String = "some default value")


//@Data
//@Accessors(fluent = true, chain = true)
// No real equivalent to `chain = true`. Can use `b.apply`.
// Generally not useful as construct objects, and define with immutable properties
data class DataWithFluent(val id: String, val other: String, var modifiable: String = "initial")

// usage
fun fluentDataExample() {
    val b = DataWithFluent("id", "other")

    b.id
    b.modifiable = "newValue"
}


// @Getter
// @Setter
class WithGetters(val prop1: String, var mutable: String)
