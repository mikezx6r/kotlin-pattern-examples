package com.bns.expressions

import com.bns.assertj.shouldEqual
import org.junit.jupiter.api.Test

class AllExpressionsKtTest {
    @Test
    fun tryExpressionTest() {
        tryIsExpression(6) shouldEqual 5
        tryIsExpression(0) shouldEqual 1
    }

    @Test
    fun ifExpressionTest() {
        ifIsExpression(5) shouldEqual 100
        ifIsExpression(10) shouldEqual 50
    }

    @Test
    fun whenIsExpression() {
        whenIsExpression("String") shouldEqual "It's a string"
        whenIsExpression("test casting") shouldEqual "It's a test value"
        whenIsExpression(12) shouldEqual "It's not in 1-10"
        whenIsExpression('a') shouldEqual "It's I don't know"
    }
}

fun tryIsExpression(n: Int): Int {
    val value = try {
        25 / n
    } catch (e: ArithmeticException) {
        0
    }
    return value + 1
}

fun ifIsExpression(n: Int): Int {
    // In Java, would use (n == 5) ? 10 : n/2.  Controversy in Kotlin Community but maintainers sticking with if/then
    val temp = if (n == 5) 10 else n / 2

    val other = if (n / 5 == 2) {
        n
    } else {
        n + 5
    }

    return temp * other
}

fun whenIsExpression(n: Any): String {
    // Can do multiple chacks and it smart casts value so can use as type if done an `is`
    // IntelliJ also tells you type it was smart cast to
    val result = when (n) {
        is String -> if (n.startsWith("test")) "a test value" else "a string"
        is Int -> if (n in 1..10) "in 1-10" else "not in 1-10"
        else -> "I don't know"
    }

    // String Templates for string and variable concatenation
    return "It's $result"
}
