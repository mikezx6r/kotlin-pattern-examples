package com.bns

import com.bns.assertj.shouldEqual
import com.bns.sealed.ProcessResult.Success
import org.junit.jupiter.api.Test

class ReifiedTypes {
    @Test
    fun test() {
        val t: Success = random(Success::class.java)
        t.id shouldEqual "id"

        val t2 = randomR<Success>()
        t2.id shouldEqual "id"

        val t3: Success = randomR()
        t3.id shouldEqual "id"

        val t4: String = randomR()
        t4 shouldEqual "bad"
    }
}


inline fun <reified T> randomR(): T = random(T::class.java)

@Suppress("UNCHECKED_CAST")
fun <T> random(clazz: Class<*>): T = when {
    clazz.isAssignableFrom(Success::class.java) -> Success("id", "other") as T
    else -> "bad" as T
}
