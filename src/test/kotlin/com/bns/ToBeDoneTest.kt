package com.bns

import com.bns.assertj.shouldThrow
import org.junit.jupiter.api.Test

class ToBeDoneTest {
    @Test
    fun `demonstrate TODO`() {
        shouldThrow<NotImplementedError> {
            ToBeDone().somethingToBeImplemented()
        }.hasMessage("An operation is not implemented: This should perform some functions")
    }
}
