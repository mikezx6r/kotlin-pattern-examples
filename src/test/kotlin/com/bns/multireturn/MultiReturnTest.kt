package com.bns.multireturn

import com.bns.assertj.isOfType
import com.bns.assertj.shouldEqual
import com.bns.sealed.ProcessResult.Success
import org.junit.jupiter.api.Test

class MultiReturnTest {
    @Test
    fun verifyTwoReturn() {
        // can take result as a Pair
        val result = returnTwoItems(12, "some string")

        result.first shouldEqual 12
        result.second shouldEqual "some string"

        // or apply decomposition to put into appropriately named variables for return
        val (number, saying) = result
        number shouldEqual 12
        saying shouldEqual "some string"
    }

    @Test
    fun verifyTripleReturn() {
        val returnValue = returnThreeItems(12, "some string", Success("id", "other"))

        returnValue.first shouldEqual 12
        returnValue.second shouldEqual "some string"

        returnValue.third.isOfType(Success::class)

        val (number, saying, result) = returnValue
        number shouldEqual 12
        saying shouldEqual "some string"
        result.isOfType(Success::class)
    }
}


fun returnTwoItems(n: Int, s: String): Pair<Int, String> = n to s

fun returnThreeItems(n: Int, s: String, a: Any): Triple<Int, String, Any> = Triple(n, s, a)
