package com.bns


// can define multiple classes in a single file. Useful for grouping small classes that are closely related,
// or for grouping domain objects into one file to ease review of structure

data class SomeDomain(val id: String) {
    init {
        check(id != "12")
    }
}

data class OtherInDomain(val other: String)


fun main() {
    SomeDomain("12")
}
