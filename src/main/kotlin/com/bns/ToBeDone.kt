package com.bns

class ToBeDone {
    fun somethingToBeImplemented() {
        // *** Mark TODO items with a function that throws a 'notimplemented' exception for you.
        // No need to add a TODO comment AND throw a NotImplemented Exception to remind you if it's called
        TODO("This should perform some functions")
    }
}
